import { Controller, Get, Param } from '@nestjs/common';

@Controller('articles')
export class ArticlesController {

    articles = [
        {id:'1'},
        {id:'2'},
        {id:'3'}
    ]
    
    @Get()
    findAll():object{
        return this.articles;
    }
    
    // localhost:3000/articles/1
    @Get(':id')
    findOne(@Param('id') id):object{
        return this.articles.find(articles => articles.id === id )
    }


}
